#!/bin/bash

##
# Bamboo build server will run this script in order to inject version and build
# information into the specified file(s).
##

matterhornHome=$1
# bamboo.repository.revision.number
revisionNumber=$2
# bamboo.buildNumber
buildNumber=$3

if [ -z "$matterhornHome" ]; then
     echo "[ERROR] Missing script argument: matterhornHome"
     exit 1
fi

# Make dir
directoryPath=${matterhornHome}/lib/build-information
rm -Rf ${directoryPath}
mkdir -p ${directoryPath}

# Write files
echo "${revisionNumber}" > "${directoryPath}/revisionNumber"
echo "${buildNumber}" > "${directoryPath}/buildNumber"

exit 0
