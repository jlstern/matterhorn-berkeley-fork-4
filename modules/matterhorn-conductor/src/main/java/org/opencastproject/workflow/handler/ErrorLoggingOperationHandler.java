/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.workflow.handler;

import org.opencastproject.job.api.JobContext;
import org.opencastproject.notify.Notifier;
import org.opencastproject.util.StringUtil;
import org.opencastproject.workflow.api.AbstractWorkflowOperationHandler;
import org.opencastproject.workflow.api.WorkflowInstance;
import org.opencastproject.workflow.api.WorkflowOperationException;
import org.opencastproject.workflow.api.WorkflowOperationResult;
import org.opencastproject.workflow.api.WorkflowOperationResult.Action;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Logs a failing workflow instance.
 */
public class ErrorLoggingOperationHandler extends AbstractWorkflowOperationHandler {

  private static final Logger logger = LoggerFactory.getLogger(ErrorLoggingOperationHandler.class);

  /** The configuration options for this handler */
  private static final SortedMap<String, String> CONFIG_OPTIONS;

  static {
    CONFIG_OPTIONS = new TreeMap<String, String>();
  }

  private Notifier notifier;

  /**
   * {@inheritDoc}
   * 
   * @see org.opencastproject.workflow.api.WorkflowOperationHandler#getConfigurationOptions()
   */
  @Override
  public SortedMap<String, String> getConfigurationOptions() {
    return CONFIG_OPTIONS;
  }

  /**
   * {@inheritDoc}
   * 
   * @see org.opencastproject.workflow.api.WorkflowOperationHandler#start(org.opencastproject.workflow.api.WorkflowInstance, JobContext)
   */
  public WorkflowOperationResult start(final WorkflowInstance workflowInstance, final JobContext context) throws WorkflowOperationException {
    logger.warn("Workflow instance failed: " + workflowInstance);
    notifyEngineeringTeam(workflowInstance, context);
    return createResult(workflowInstance.getMediaPackage(), Action.CONTINUE);
  }

  public void notifyEngineeringTeam(final WorkflowInstance w, final JobContext context) {
    final StringBuilder emailMessage = new StringBuilder();
    final ToStringStyle toStringStyle = ToStringStyle.MULTI_LINE_STYLE;
    if (context != null) {
      final ToStringBuilder b = new ToStringBuilder(context, toStringStyle)
          .append("id", context.getId()).append("properties", StringUtil.toString(context.getProperties()))
          .append('\n').append('\n');
      emailMessage.append(b.toString());
    }
    if (w != null) {
      final ToStringBuilder b = new ToStringBuilder(w, toStringStyle)
          .append("id", w.getId()).append("title", w.getTitle()).append("description", w.getDescription())
          .append("operations", w.getOperations()).append("currentOperation", w.getCurrentOperation())
          .append("state", w.getState()).append("mediaPackage", w.getMediaPackage())
          .append("getErrorMessages", ArrayUtils.toString(w.getErrorMessages()));
      emailMessage.append(b.toString());
    }
    final String message = emailMessage.toString();
    final String subject = this.getClass().getSimpleName().concat(": ").concat(message);
    notifier.notifyEngineeringTeam(subject, message);
  }

  public void setNotifier(final Notifier notifier) {
    this.notifier = notifier;
  }
}
