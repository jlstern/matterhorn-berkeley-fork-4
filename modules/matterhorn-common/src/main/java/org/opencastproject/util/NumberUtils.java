/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.util;

import org.apache.commons.lang.StringUtils;

import java.util.LinkedList;
import java.util.List;

/**
 * @author John Crossman
 */
public final class NumberUtils {

  private NumberUtils() {
  }

  /**
   * @param longAsString Null allowed. Expected to be string representation of number.
   * @param defaultValue fallback when first arg is null
   * @return first arg as long or default when first arg is null
   */
  public static long getLong(final String longAsString, final long defaultValue) {
    final String value = StringUtils.trimToNull(longAsString);
    return (value == null) ? defaultValue : new Long(value);
  }

  public static List<Integer> range(final int first, final int last) {
    if (last < first) {
      throw new IllegalArgumentException("Invalid range: [" + first + ',' + last + ']');
    }
    final List<Integer> range = new LinkedList<Integer>();
    for (int counter = first; counter <= last; counter++) {
      range.add(counter);
    }
    return range;
  }
}
