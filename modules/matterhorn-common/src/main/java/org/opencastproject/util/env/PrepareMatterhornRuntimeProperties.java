/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.util.env;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.impl.cookie.DateUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Properties;

/**
 * This class is run by the shell script: bin/prepare_matterhorn_properties.sh
 * @author John Crossman
 */
public final class PrepareMatterhornRuntimeProperties {

  /**
   * Output to user when script args are invalid or missing.
   */
  private static final String usage = "";

  private static final String[] SYSTEM_PROPERTY_FILES = new String[] {
      "etc/config.properties", "etc/system.properties"
  };

  private static final String[] SERVICE_PROPERTY_FILES = new String[] {
      "etc/services/org.ops4j.pax.logging.properties"
  };

  /**
   * Private.
   */
  private PrepareMatterhornRuntimeProperties() {
  }

  /**
   * Invoked by shell script, command-line.
   * @param args expects one arg: FELIX_HOME per start_matterhorn script.
   * @throws IOException when file read/write fails.
   */
  public static void main(final String[] args) throws IOException {
    final StringBuilder errorMsg = new StringBuilder();
    if (ArrayUtils.getLength(args) == 1) {
      final File baseDirectory = new File(args[0]);
      if (!baseDirectory.exists()) {
        errorMsg.append("    - Base directory (").append(baseDirectory.getAbsolutePath()).append(") does not exist.");
      } else {
        final File targetDirectory = new File(baseDirectory, "target");
        for (final String filePath : SYSTEM_PROPERTY_FILES) {
          final File sourceFile = new File(baseDirectory, filePath);
          final File targetFile = new File(targetDirectory, filePath);
          setWritable(targetFile);
          processProperties(sourceFile, targetFile);
        }
        //
        for (final String filePath : SERVICE_PROPERTY_FILES) {
          final String relativePath = StringUtils.replace(filePath, ".properties", "_" + EnvironmentUtil.getEnvironment() + ".properties");
          final File sourceFile = new File(baseDirectory, relativePath);
          if (sourceFile.exists()) {
            final File targetFile = new File(baseDirectory, filePath);
            if (canWrite(targetFile)) {
              FileUtils.copyFile(sourceFile, targetFile);
            } else {
              errorMsg.append("    - ")
                  .append(ClassUtils.getShortClassName(PrepareMatterhornRuntimeProperties.class))
                  .append(" does not have permission to write to ")
                  .append(targetFile.getAbsolutePath());
            }
          } else {
            console("Environment-specific file (", sourceFile.getAbsolutePath(), ") not found. Using default, ", filePath);
          }
        }
      }
    } else {
      errorMsg.append("    - ")
          .append(ClassUtils.getShortClassName(PrepareMatterhornRuntimeProperties.class))
          .append(" expects one argument: FELIX_HOME (absolute path)");
    }
    if (errorMsg.length() > 0) {
      System.err.println("\n[ERROR]\n\n" + errorMsg.toString() + "\n\n" + usage + "\n\n");
    }
  }

  private static void setWritable(final File file) {
    if (file.getParentFile().exists()) {
      file.getParentFile().setWritable(true);
    }
    if (file.exists()) {
      file.setWritable(true);
    }
  }

  /**
   * Logging to {@link System#out}
   * @param messages to be concatenated.
   */
  private static void console(final String... messages) {
    for (final String message : messages) {
      System.out.print(message);
    }
    System.out.println();
  }

  /**
   * @param file Null not allowed.
   * @return true if {@link java.io.File#canWrite()}
   */
  private static boolean canWrite(final File file) {
    final File parentFile = file.getParentFile();
    return parentFile.exists() && parentFile.canWrite() && (!file.exists() || file.canWrite());
  }

  /**
   * This method will:
   * <ol>
   *   <li>Clean. I.e., delete target file, if exists.</li>
   *   <li>Load properties in {@link org.opencastproject.util.env.EnvironmentUtil#getRuntimePropertyOverrides()}.</li>
   *   <li>Load properties in sourceFile.</li>
   *   <li>Perform token replacement in sourceFile using values of {@link PropertyOverride#matterhornEnvironment})
   *        and {@link PropertyOverride#matterhornProfile}.</li>
   *   <li>Load modified sourceFile as {@link java.util.Properties} instance.</li>
   *   <li>Override properties in source using runtime-properties-override</li>
   *   <li>Write resulting properties to targetFile location.</li>
   * </ol>
   * @param sourceFile Not null and exists.
   * @param targetFile Not null and guaranteed write-able.
   */
  private static void processProperties(final File sourceFile, final File targetFile) throws IOException {
    FileUtils.deleteQuietly(targetFile);
    final Properties sourceProperties = new Properties();
    sourceProperties.load(new FileInputStream(sourceFile));
    // Unfortunately, we need a Properties instance so we can write to a file.
    final Dictionary targetDictionary = new EProperties(sourceProperties);
    final Properties targetProperties = new Properties();
    final Enumeration keys = targetDictionary.keys();
    while (keys.hasMoreElements()) {
      final Object next = keys.nextElement();
      if (next instanceof String) {
        final String key = (String) next;
        targetProperties.setProperty(key, (String) targetDictionary.get(key));
      }
    }
    final File parentFile = targetFile.getParentFile();
    if (!parentFile.exists()) {
      if (!parentFile.mkdirs()) {
        throw new IOException("Failed to create directory: " + parentFile.getAbsolutePath());
      }
    }
    final OutputStream out = new FileOutputStream(targetFile);
    final String date = DateUtils.formatDate(new Date());
    targetProperties.store(out, "This file was generated dynamically on "
      + date
      + " using source properties file "
      + sourceFile.getAbsolutePath()
      + ", env = "
      + EnvironmentUtil.getEnvironment()
      + " and profile = "
      + EnvironmentUtil.getApplicationProfile());
  }

}
