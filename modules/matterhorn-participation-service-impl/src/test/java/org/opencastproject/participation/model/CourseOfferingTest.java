/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.sforce.soap.partner.sobject.SObject;
import junit.framework.Assert;

import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;
import org.junit.Test;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author John Crossman
 */
public class CourseOfferingTest {

  @Test
  public void testCourseOfferingSerialization() throws Exception {
    ObjectMapper mapper = new ObjectMapper();
    AnnotationIntrospector introspector = new JaxbAnnotationIntrospector();
    // Serializer use JAXB annotations (only)
    mapper.getSerializationConfig().withAnnotationIntrospector(introspector);
    StringWriter sw = new StringWriter();
    CourseOffering thisCourseOffering = buildCourseOffering();
    mapper.writeValue(sw, thisCourseOffering);
    StringReader sr = new StringReader(sw.toString());
    CourseOffering secondCourseOffering = mapper.readValue(sr, CourseOffering.class);
    Assert.assertEquals(thisCourseOffering, secondCourseOffering);
  }

  @Test
  public void testEquals() {
    final CourseOffering c = buildCourseOffering();
    final Term term = c.getTerm();
    //
    final CourseData courseData = new CourseData();
    final CanonicalCourse cc = new CanonicalCourse();
    cc.setCcn(c.getCanonicalCourse().getCcn());
    courseData.setCanonicalCourse(cc);
    final Term termClone = new Term(term.getSemester(), term.getTermYear(), term.getSemesterStartDate(), term.getSemesterEndDate());
    courseData.setTerm(termClone);
    // Assert
    assertEquals(c, courseData);
    // Test in context of a Map
    final Map<CourseData, SObject> map = new HashMap<CourseData, SObject>();
    map.put(c, new SObject());
    assertTrue(map.containsKey(courseData));
  }

  @Test
  public void testNotEquals() {
    final CourseOffering c = buildCourseOffering();
    final Term term = c.getTerm();
    //
    final CourseData courseData = new CourseData();
    courseData.setTerm(new Term(term.getSemester(), term.getTermYear() + 1, term.getSemesterStartDate(), term.getSemesterEndDate()));
    assertFalse(c.equals(courseData));
  }

  private CourseOffering buildCourseOffering() {
    final CourseOffering courseOffering = new CourseOffering();
    courseOffering.setSalesforceID("1234567");
    courseOffering.setCourseOfferingId("2013B54321");

    CanonicalCourse canonicalCourse = new CanonicalCourse();
    canonicalCourse.setCcn(54321);
    canonicalCourse.setDepartment("ETS");
    canonicalCourse.setTitle("SF Giants Baseball 101");
    courseOffering.setCanonicalCourse(canonicalCourse);
    //
    final Term term = new Term();
    term.setSemester(Semester.Spring);
    term.setTermYear(2013);
    term.setSemesterStartDate("2014-01-01");
    term.setSemesterEndDate("2014-05-30");
    courseOffering.setTerm(term);
    courseOffering.setSection("001");

    List<DayOfWeek> meetingDays = new LinkedList<DayOfWeek>();
    meetingDays.add(DayOfWeek.Tuesday);
    meetingDays.add(DayOfWeek.Thursday);
    courseOffering.setMeetingDays(meetingDays);

    courseOffering.setStartTime("0900");
    courseOffering.setEndTime("1000");

    Room room = new Room();
    room.setBuilding("Pimentel");
    room.setCapability(RoomCapability.screencastAndVideo);
    room.setSalesforceID("123456");
    room.setRoomNumber("1");
    courseOffering.setRoom(room);

    courseOffering.setScheduled(false);
    courseOffering.setStageOfApproval(StageOfApproval.approved);
    courseOffering.setStageOfLifecycle(SalesforceProjectLifecycle.prospecting);

    Participation participation1 = new Participation();
    participation1.setApproved(true);
    Instructor instructor1 = new Instructor();
    instructor1.setCalNetUID("212386");
    instructor1.setDepartment("ETS");
    instructor1.setSalesforceID("98876554");
    instructor1.setEmail("posey@bekeley.edu");
    instructor1.setFirstName("Buster");
    instructor1.setLastName("Posey");
    instructor1.setRole("Faculty");
    participation1.setInstructor(instructor1);
    //
    Participation participation2 = new Participation();
    participation2.setApproved(false);
    Instructor instructor2 = new Instructor();
    instructor2.setCalNetUID("322587");
    instructor2.setDepartment("ETS");
    instructor2.setSalesforceID("1234567");
    instructor2.setEmail("lincecum@bekeley.edu");
    instructor2.setFirstName("Tim");
    instructor2.setLastName("Lincecum");
    instructor2.setRole("Faculty");
    participation2.setInstructor(instructor2);
    //
    final Set<Participation> participationList = new HashSet<Participation>();
    participationList.add(participation1);
    participationList.add(participation2);
    courseOffering.setParticipationSet(participationList);

    CapturePreferences capPrefs = new CapturePreferences();
    capPrefs.setRecordingType(RecordingType.videoAndScreencast);
    capPrefs.setRecordingAvailability(RecordingAvailability.publicCreativeCommons);
    capPrefs.setDelayPublishByDays(0);
    courseOffering.setCapturePreferences(capPrefs);

    return courseOffering;
  }

}
