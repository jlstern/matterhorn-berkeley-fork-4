/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import org.opencastproject.participation.impl.MVUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;

import static org.easymock.EasyMock.expect;

/**
 * @author John Crossman
 */
public abstract class AbstractResultConverterTest {

  protected void expectReturn(final ResultSet r, final MVUtils.StrCol column) throws SQLException {
    expectReturn(r, column, column.name());
  }

  protected void expectReturn(final ResultSet r, final MVUtils.StrCol column, final String expectedValue) throws SQLException {
    expect(r.getString(column.name())).andReturn(expectedValue).once();
  }

  protected void expectReturn(final ResultSet r, final MVUtils.IntCol column, final Integer expectedValue) throws SQLException {
    expect(r.getInt(column.name())).andReturn(expectedValue).once();
  }

  protected void expectReturn(final ResultSet r, final MVUtils.DateCol column, final Date date) throws SQLException {
    expect(r.getDate(column.name())).andReturn(date).once();
  }

}
