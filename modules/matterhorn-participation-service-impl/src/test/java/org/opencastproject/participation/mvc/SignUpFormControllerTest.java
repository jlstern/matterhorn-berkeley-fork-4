/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.mvc;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.opencastproject.participation.api.CourseManagementService;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.RecordingAvailability;
import org.opencastproject.participation.model.RecordingType;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.RoomCapability;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.StageOfApproval;
import org.opencastproject.participation.model.Term;
import org.opencastproject.salesforce.SalesforceFieldInstructor;
import org.opencastproject.security.api.DefaultOrganization;
import org.opencastproject.security.api.Organization;
import org.opencastproject.security.api.SecurityService;
import org.opencastproject.security.api.User;
import org.opencastproject.util.data.Collections;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.ServletException;
import java.io.IOException;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.reset;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author John Crossman
 */
@Ignore
public class SignUpFormControllerTest {

  private SignUpFormController controller;
  private CourseOffering course;
  private SecurityService securityService;
  private final Organization authorizedOrg = new DefaultOrganization();
  private final User authorizedUser = new User("calNetUID1", authorizedOrg.getId(), new String[]{"ROLE_EMPLOYEE-TYPE-STAFF"});

  private MockHttpServletRequest request;
  private MockHttpServletResponse response;

  @Before
  public void before() throws IOException {
    course = new CourseOffering();
    course.setCanonicalCourse(new CanonicalCourse(1234, "Title", "Department"));
    final Term term = new Term(Semester.Fall, 2014, "2014-08-22", "2014-12-05");
    course.setTerm(term);
    course.setRoom(new Room("salesforceID", "building", "roomNumber", RoomCapability.screencastAndVideo));
    course.setMeetingDays(Collections.list(DayOfWeek.Monday, DayOfWeek.Wednesday, DayOfWeek.Friday));
    course.setStartTime("1830");
    course.setEndTime("1900");
    final String calNetUID1 = "calNetUID1";
    final Instructor i1 = new Instructor(calNetUID1);
    i1.setFirstName("Lindsey");
    i1.setLastName("Buckingham");
    final Participation p1 = new Participation(SalesforceFieldInstructor.Instructor_1, i1, false);
    final Instructor i2 = new Instructor("calNetUID2");
    i2.setFirstName("Stevie");
    i2.setLastName("Nicks");
    course.setCapturePreferences(new CapturePreferences(RecordingType.audioOnly, RecordingAvailability.studentsOnly, 1));
    final Participation p2 = new Participation(SalesforceFieldInstructor.Instructor_2, i2, true);
    course.setParticipationSet(Collections.set(p1, p2));
    course.setStageOfApproval(StageOfApproval.partiallyApproved);
    course.setCourseOfferingId(term.getTermYear() + term.getSemester().getTermCode() + course.getCanonicalCourse().getCcn());
    //
    controller = new SignUpFormController();
    final CourseManagementService courseManagementService = createMock(CourseManagementService.class);
    expect(courseManagementService.getCourseOffering(course.getCourseOfferingId())).andReturn(course).anyTimes();
    expect(courseManagementService.getCourseOffering(anyObject(String.class))).andReturn(null).anyTimes();
    replay(courseManagementService);
    //
    securityService = createMock(SecurityService.class);
    resetAndReplay(securityService, authorizedUser.getUserName(), authorizedUser.getRoles());
    //
    controller.setCourseManagementService(courseManagementService);
    controller.setSecurityService(securityService);
    //
    request = new MockHttpServletRequest();
    response = new MockHttpServletResponse();
    response.setOutputStreamAccessAllowed(true);
  }

  @Test
  public void testAdminUser() throws ServletException, IOException {
    resetAndReplay(securityService, "adminUID", DefaultOrganization.DEFAULT_ORGANIZATION_ADMIN);
    request.setParameter("id", course.getCourseOfferingId());
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertTrue(content.contains(course.getCanonicalCourse().getTitle()));
  }

  @Test
  public void testUnauthorizedUser() throws ServletException, IOException {
    resetAndReplay(securityService, "unauthorizedUser", "ROLE_EMPLOYEE-TYPE-STAFF");
    request.setParameter("id", course.getCourseOfferingId());
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertFalse(content.contains(course.getCanonicalCourse().getTitle()));
    assertTrue(content.contains("you are not authorized"));
    assertTrue(content.contains("contact"));
  }

  @Test
  public void testMissingParameter() throws ServletException, IOException {
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertTrue(content.contains("Sign Up"));
    assertTrue(content.contains("does not contain a course identifier"));
    assertTrue(content.contains("ets_logo.png"));
  }

  @Test
  public void testNoSuchCourse() throws ServletException, IOException {
    request.setParameter("id", "noSuchCourse");
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertTrue(content.contains("Our system has no record"));
  }

  @Test
  public void testHeaderInfo() throws ServletException, IOException {
    request.setParameter("id", course.getCourseOfferingId());
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertTrue(content.contains(course.getCanonicalCourse().getTitle()));
    assertTrue(content.contains(course.getRoom().getBuilding()));
    assertTrue(content.contains(course.getRoom().getRoomNumber()));
    assertTrue(content.contains("Monday, Wednesday, Friday "));
    assertTrue(content.contains("6:30PM-7:00PM"));
    final int indexOfLindsey = content.indexOf("Lindsey Buckingham");
    assertTrue(indexOfLindsey > -1);
    assertTrue(indexOfLindsey < content.indexOf("(not yet signed up)"));
    assertTrue(content.contains("Your co-instructor signed up with the following settings"));
    assertTrue(content.contains("Audio only"));
    assertTrue(content.contains("Students in your course only with no license to redistribute"));
    assertTrue(content.contains("1 day after capture"));
  }

  @Test
  public void testNoPublishDelay() throws ServletException, IOException {
    course.setCapturePreferences(new CapturePreferences(RecordingType.videoAndScreencast, RecordingAvailability.publicNoRedistribute, 0));
    request.setParameter("id", course.getCourseOfferingId());
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertTrue(content.contains("Video, Computer Screen, and Audio"));
    assertTrue(content.contains("<strong>Public</strong> with no license to redistribute"));
    assertTrue(content.contains("As soon after capture as possible"));
  }

  @Test
  public void testAdminScheduleOverride() throws ServletException, IOException {
    course.setAdminScheduleOverride("1022796");
    course.setScheduled(true);
    request.setParameter("id", course.getCourseOfferingId());
    controller.doGet(request, response);
    final String content = new String(response.getContentAsByteArray());
    assertTrue(content.contains("The following settings have been selected for capture, and the recordings have been scheduled"));
  }

  private void resetAndReplay(final SecurityService securityService, final String calNetUID, final String... roles) {
    final User user = new User(calNetUID, authorizedOrg.getId(), roles);
    reset(securityService);
    expect(securityService.getUser()).andReturn(user).once();
    expect(securityService.getOrganization()).andReturn(authorizedOrg).once();
    replay(securityService);
  }

}
