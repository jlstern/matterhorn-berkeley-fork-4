/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.salesforce;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;

import java.util.List;
import java.util.Map;

/**
 * @author John King
 */
public interface SalesforceConnectorService {

  /**
   * @param queryType type of Salesforce query.
   * @param args zero or more args to populate query. Null allowed.
   * @return list of results.
   */
  QueryResult query(SalesforceQueryType queryType, Map<String, ?> args);

  /**
   * @see com.sforce.soap.partner.PartnerConnection#update(com.sforce.soap.partner.sobject.SObject[])
   * @param sObjects one or more records
   * @return results, if any
   */
  SaveResult[] update(SObject... sObjects);

  /**
   * @see com.sforce.soap.partner.PartnerConnection#upsert(String, com.sforce.soap.partner.sobject.SObject[])
   * @param sObjectList never null
   * @return per {@link com.sforce.soap.partner.PartnerConnection}
   * @throws ConnectionException when fail to connect
   */
  SaveResult[] create(final List<SObject> sObjectList) throws ConnectionException;

}
