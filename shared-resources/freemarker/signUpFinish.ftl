<#include "macros.ftl" >

<#assign title><@message "${finishPage.pageTitle}" /></#assign>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>${title}</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="shortcut icon" href="/img/favicon.ico" />
        <link href="http://localhost/~john/matterhorn/css/sign-up.css" rel="stylesheet" type="text/css" />
        <link href="/css/jquery/jquery.fancybox.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="/js/angular/angular.min.js"></script>
        <script type="text/javascript" src="http://localhost/~john/matterhorn/js/sign-up.js"></script>
    </head>
    <body ng-app="">
        <div class="cct-container-main">
            <div id="ets_logo"><img src="/img/mh_logos/ets_logo.png"/></div>
            <h1>${title}</h1>
            <#if finishPage.pageBody??>
                <br/>
                <div class="major-section">
                    ${finishPage.pageBody}
                </div>
            </#if>
        </div>
    </body>
</html>
